package digitalturbine.adparser.module;

import digitalturbine.adparser.MainActivity;
import digitalturbine.adparser.api.DTurbineApiService;
import digitalturbine.adparser.model.AdsDataContent;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class GetPresenter {
    private final String BASE_URL = "http://ads.appia.com/getAds?id=236&password=OVUJ1DJN&siteId=4288&deviceId=4230&sessionId=techtestsession&totalCampaignsRequested=10";
    private final String FOLLOW_FIX = "&lname=VU/";
    MainActivity mView;
    DTurbineApiService mApi;

    public GetPresenter(MainActivity view) {
        this.mView = view;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ads.appia.com/")
                .client(new OkHttpClient.Builder().build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        mApi = retrofit.create(DTurbineApiService.class);

    }

    public void loadRepositories() {
        mApi.getAdsList(BASE_URL + FOLLOW_FIX).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<AdsDataContent>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        mView.onRequestFail(e.toString());
                    }

                    @Override
                    public void onNext(Response<AdsDataContent> response) {
                        mView.onRequestSuccess(response.body().getListAd());
                    }
                });
    }
}