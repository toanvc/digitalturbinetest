package digitalturbine.adparser.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import digitalturbine.adparser.R;
import digitalturbine.adparser.model.AdItem;

/**
 * Created by Toan Vu on 7/20/16.
 */
public class AdAdapter extends RecyclerView.Adapter<AdAdapter.MyViewHolder> {
    private static final String TAG = "AdAdapter";
    private ArrayList<AdItem> mAdList;

    private Activity mActivity;


    public AdAdapter(Activity activity, ArrayList<AdItem> adList) {
        this.mActivity = activity;
        this.mAdList = adList;
    }


    public void setAdsList(ArrayList<AdItem> list) {
        this.mAdList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ad_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AdItem data = mAdList.get(position);
        bindData(data, holder);
    }

    @Override
    public int getItemCount() {
        return mAdList.size();
    }

    private void bindData(final AdItem data, MyViewHolder holder) {
        holder.parentView.setTag(holder);
        holder.name.setText(data.getProductName());
        holder.description.setText(data.getProductDescription());
        holder.category.setText(data.getCategoryName());
        holder.numberOfRate.setText(data.getNumberOfRatings());

        Glide.with(mActivity)
                .load(data.getProductThumbnail())
                .placeholder(R.drawable.placeholder)
                .into(holder.thumbnail);

        //now we hide this feature, just comment code, delete later
        holder.parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = data.getClickProxyURL();
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                openUrl(url);
            }
        });

    }

    private void openUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        mActivity.startActivity(i);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        public TextView name;
        @BindView(R.id.description)
        public TextView description;
        @BindView(R.id.number_of_rate)
        public TextView numberOfRate;
        @BindView(R.id.category)
        public TextView category;
        @BindView(R.id.thumbnail)
        public ImageView thumbnail;
        public View parentView;

        public MyViewHolder(View view) {
            super(view);
            this.parentView = view;

            try {
                ButterKnife.bind(this, view);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void onRelease() {
        if (mAdList != null) {
            mAdList.clear();
            mAdList = null;
        }
    }
}