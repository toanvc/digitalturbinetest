package digitalturbine.adparser.api;

import digitalturbine.adparser.model.AdsDataContent;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Toan Vu on 7/20/16.
 */
public interface DTurbineApiService {
    @GET
    Observable<Response<AdsDataContent>> getAdsList(@Url String url);
}