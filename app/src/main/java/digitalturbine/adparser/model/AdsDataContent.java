package digitalturbine.adparser.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by Toan Vu on 7/20/16.
 */
@Root(name = "ads")
public class AdsDataContent {
    @ElementList(inline = true)
    private ArrayList<AdItem> listAd;
    @Element(name = "responseTime")
    private String responseTime;
    @Element(name = "serverId")
    private String serverId;
    @Element(name = "totalCampaignsRequested")
    private long totalCampaignsRequested;
    @Element(name = "version")
    private String version;


    public ArrayList<AdItem> getListAd() {
        return listAd;
    }

    public void setListAd(ArrayList<AdItem> listAd) {
        this.listAd = listAd;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public long getTotalCampaignsRequested() {
        return totalCampaignsRequested;
    }

    public void setTotalCampaignsRequested(long totalCampaignsRequested) {
        this.totalCampaignsRequested = totalCampaignsRequested;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
