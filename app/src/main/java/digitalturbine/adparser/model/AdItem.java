package digitalturbine.adparser.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "ad", strict = false)
public class AdItem {
    @Element(name = "appId")
    private String appId;
    @Element(name = "productName")
    private String productName;
    @Element(name = "productThumbnail")
    private String productThumbnail;

    @Element(name = "categoryName")
    private String categoryName;
    @Element(name = "minOSVersion", required = false)
    private String minOSVersion;

    @Element(name = "numberOfRatings")
    private String numberOfRatings;
    @Element(name = "productDescription")
    private String productDescription;
    @Element(name = "clickProxyURL")
    private String clickProxyURL;
    @Element(name = "impressionTrackingURL")
    private String impressionTrackingURL;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductThumbnail() {
        return productThumbnail;
    }

    public void setProductThumbnail(String productThumbnail) {
        this.productThumbnail = productThumbnail;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getMinOSVersion() {
        return minOSVersion;
    }

    public void setMinOSVersion(String minOSVersion) {
        this.minOSVersion = minOSVersion;
    }

    public String getNumberOfRatings() {
        return numberOfRatings;
    }

    public void setNumberOfRatings(String numberOfRatings) {
        this.numberOfRatings = numberOfRatings;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getClickProxyURL() {
        return clickProxyURL;
    }

    public void setClickProxyURL(String clickProxyURL) {
        this.clickProxyURL = clickProxyURL;
    }

    public String getImpressionTrackingURL() {
        return impressionTrackingURL;
    }

    public void setImpressionTrackingURL(String impressionTrackingURL) {
        this.impressionTrackingURL = impressionTrackingURL;
    }
}