package digitalturbine.adparser;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import digitalturbine.adparser.adapter.AdAdapter;
import digitalturbine.adparser.model.AdItem;
import digitalturbine.adparser.module.GetPresenter;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.recycle_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.text)
    TextView mTextContent;

    AdAdapter mAdapter;
    GetPresenter mGetPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mAdapter = new AdAdapter(this, new ArrayList<AdItem>());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        initSwipeLayout();

        mGetPresenter = new GetPresenter(this);
        requestData(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.onRelease();
        mAdapter = null;
        mGetPresenter = null;
    }

    public void onRequestSuccess(ArrayList<AdItem> listAds) {
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
        mTextContent.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);

        mAdapter.setAdsList(listAds);
        mAdapter.notifyDataSetChanged();
    }

    public void onRequestFail(String message) {
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mTextContent.setVisibility(View.VISIBLE);
        mTextContent.setText(String.format(getString(R.string.fail_request), message));

    }

    private void requestData(boolean isRefresh) {
        if (!isRefresh)
            mProgressBar.setVisibility(View.VISIBLE);
        mGetPresenter.loadRepositories();
    }

    private void initSwipeLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestData(true);
            }
        });
    }
}
